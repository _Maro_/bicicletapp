var Bicicleta = require('../models/bicicleta-model');

exports.bicicleta_list = function (req, res) {
    res.render('bicicleta/index', {bicicletas: Bicicleta.allBicicletas});
}

exports.bicicleta_create_get = function (req, res) {
    res.render('bicicleta/create');
}

exports.bicicleta_create_post = function (req, res) {
    var bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bicicleta.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bicicleta);

    res.redirect('/bicicleta');
}

exports.bicicleta_update_get = function (req, res) {
    var bicicleta = Bicicleta.findById(req.params.id);

    res.render('bicicleta/update', {bicicleta});
}

exports.bicicleta_update_post = function (req, res) {
    var bicicleta = Bicicleta.findById(req.params.id);
    bicicleta.color = req.body.color;
    bicicleta.modelo = req.body.modelo;
    bicicleta.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/bicicleta');
}

exports.bicicleta_delete_post = function (req, res) {
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicleta');
}

