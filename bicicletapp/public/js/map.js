var mymap = L.map('main_map').setView([-34.708281,-58.391451], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

$.ajax({
    dataType: 'json',
    url: 'api/bicicleta',
    success: function (result) {
        console.log(result);
        result.bicicletas.forEach(function (bicicleta) {
            L.marker(bicicleta.ubicacion, {title: bicicleta.id}).addTo(mymap);
        })
    }
})