var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function () {
    return 'ID: ' + this.id + ' | Color: ' + this.color;
}

Bicicleta.allBicicletas = [];
Bicicleta.add = function (bicicleta) {
    Bicicleta.allBicicletas.push(bicicleta);
}

Bicicleta.findById = function (bicicletaId) {
    var bicicleta = Bicicleta.allBicicletas.find(x => x.id == bicicletaId);
    if (bicicleta) {
        return bicicleta;
    } else {
        throw new Error('No existe una bicicleta con el ID ' + bicicletaId);
    }
}

Bicicleta.removeById = function (bicicletaId) {
    Bicicleta.findById(bicicletaId);
    for (var i = 0; i < Bicicleta.allBicicletas.length; i++){
        if (Bicicleta.allBicicletas[i].id == bicicletaId) {
            Bicicleta.allBicicletas.splice(i, 1);
            break;
        }
    }
}

var a = new Bicicleta(1, 'Celeste', 'Urbana', [-34.708281,-58.381451]);
var b = new Bicicleta(2, 'Verde', 'Montaña', [-34.708281,-58.401451]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;
