var Bicicleta = require('../../models/bicicleta-model');

exports.bicicleta_list = function (req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicicletas
    });
}

exports.bicicleta_create = function (req, res) {
    var bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bicicleta.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bicicleta);

    res.status(200).json({
        bicicleta: bicicleta
    });
}

exports.bicicleta_update = function (req, res) {
    var bicicleta = Bicicleta.findById(req.body.id);
    bicicleta.color = req.body.color;
    bicicleta.modelo = req.body.modelo;
    bicicleta.ubicacion = [req.body.lat, req.body.lng];

    res.status(200).json({
        bicicleta: bicicleta
    });
}

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeById(req.body.id);

    res.status(200).json({
        message: "Bicicleta eliminada."
    });
}